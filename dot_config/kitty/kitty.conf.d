shell                           .
#term                           xterm-256color
wayland_titlebar_color          #303030
font_size                       14
font_family                     BlexMono Nerd Font
bold_font                       auto
italic_font                     auto
bold_italic_font                auto

cursor_shape                    block

scrollback_lines                2000
scrollback_pager_history_size   10
scrollback_pager                less --chop-long-lines --RAW-CONTROL-CHARS +INPUT_LINE_NUMBER



enable_audio_bell               no
visual_bell_duration            0.1
visual_bell_color               #44475A

window_margin_width             5
background_opacity              0.45
window_logo_path                kitty-app-light-64.png
window_logo_position            top-right
remember_window_size            yes
# BEGIN_KITTY_THEME
# Dracula
#include                        base16-dracula-theme.conf
# END_KITTY_THEME
