return {
  "nvim-treesitter/nvim-treesitter",
  opts = function(_, opts)
    -- add tsx and treesitter
    vim.list_extend(opts.ensure_installed, {
      "bash",
      "css",
      "gitcommit",
      "gitignore",
      "html",
      "javascript",
      "json",
      "jsonc",
      "lua",
      "markdown",
      "markdown_inline",
      "python",
      "query",
      "rasi",
      "regex",
      "toml",
      "tsx",
      "typescript",
      "vim",
      "yaml",
    })
  end,
}
