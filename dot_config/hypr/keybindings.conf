
#   █▄▀ █▀▀ █▄█ █▄▄ █ █▄░█ █▀▄ █ █▄░█ █▀▀ █▀ ░ █▀▀ █▀█ █▄░█ █▀▀
#   █░█ ██▄ ░█░ █▄█ █ █░▀█ █▄▀ █ █░▀█ █▄█ ▄█ ▄ █▄▄ █▄█ █░▀█ █▀░
# See https://wiki.hyprland.org/Configuring/Keywords/ for more
# see https://wiki.hyprland.org/Configuring/Binds/ for more

$mainMod                    = SUPER

#   █▀█ █▀█ █▀█ █▀▀ █▀█ ▄▀█ █▀▄▀█ █▀
#   █▀▀ █▀▄ █▄█ █▄█ █▀▄ █▀█ █░▀░█ ▄█
# default programs
$BROWSER                    = firefox
$YOUTUBE                    = /opt/thorium-browser/thorium-browser --user-data-dir=/home/christian/.local/share/webapps/thorium-youtube --profile-directory=Default --app-id=agimnkijcaahngcdmfeangaknmldooml
$EDITOR                     = kitty --class Neovim --title Neovim nvim
$FILEMANAGER                = nautilus
$MEDIAPLAYER                = vlc
$TERMINAL                   = kitty

#   █▀▄▀█ █ █▀ █▀▀
#   █░▀░█ █ ▄█ █▄▄
bind = $mainMod, F, exec, $BROWSER # Open BROWSER
bind = $mainMod, Y, exec, $YOUTUBE #  Open YouTube
bind = $mainMod, E, exec, $EDITOR # Show text-editor
bind = $mainMod, RETURN, exec, $TERMINAL # Open the terminal
bind = $mainMod_SHIFT, RETURN, exec, $FILEMANAGER # Open the filemanager
bind = $mainMod, L, exec, swaylock # Lock the screen
bind = $mainMod, M, exec, wlogout --protocol layer-shell -b 4 -T 400 -B 400 # show the logout window
bind = $mainMod, T, exec, ~/.config/HyprV/hyprv_util vswitch # switch HyprV version
bind = $mainMod, SPACE, exec,  killall wofi || wofi # Show the graphical app launcher
bind = ALT, V, exec, cliphist list | wofi -dmenu | cliphist decode | wl-copy # open clipboard manager
bind = $mainMod, R, exec,  killall rofi || /home/christian/.config/rofi/launchers/type-1/launcher.sh
bind = $mainMod_SHIFT, F4, exec, /home/christian/toggle_touchpad

#   █░█░█ █ █▄░█ █▀▄ █▀█ █░█░█   █▀▄▀█ ▄▀█ █▄░█ ▄▀█ █▀▀ █▀▄▀█ █▀▀ █▄░█ ▀█▀
#   ▀▄▀▄▀ █ █░▀█ █▄▀ █▄█ ▀▄▀▄▀   █░▀░█ █▀█ █░▀█ █▀█ █▄█ █░▀░█ ██▄ █░▀█ ░█░
bind = $mainMod_SHIFT, F, fullscreen,0
bind = $mainMod, Q, killactive # close the active window
bind = $mainMod, V, togglefloating # Allow a window to float
bind = $mainMod, J, togglesplit # dwindle
bind = $mainMod, P, pseudo
bind = $mainMod SHIFT, M, exit # Exit Hyprland all together no (force quit Hyprland)
bind = $mainMod, S, exec, grim -g "$(slurp)" - | swappy -f - # take a screenshot
bind = $mainMod_SHIFT, R, exec, hyprctl reload # Reload Hyprland
bind = ALT, Tab, cyclenext

#   █▀▀ █▀█ █▀▀ █░█ █▀
#   █▀░ █▄█ █▄▄ █▄█ ▄█
# Move focus with mainMod + arrow keys
bind = $mainMod, left, movefocus, l
bind = $mainMod, right, movefocus, r
bind = $mainMod, up, movefocus, u
bind = $mainMod, down, movefocus, d

#   █▀ █░█░█ █ ▀█▀ █▀▀ █░█
#   ▄█ ▀▄▀▄▀ █ ░█░ █▄▄ █▀█
# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
bind = $mainMod, 0, workspace, 10

#   █▀▄▀█ █▀█ █░█ █▀▀
#   █░▀░█ █▄█ ▀▄▀ ██▄
# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, 1, movetoworkspacesilent, 1
bind = $mainMod SHIFT, 2, movetoworkspacesilent, 2
bind = $mainMod SHIFT, 3, movetoworkspacesilent, 3
bind = $mainMod SHIFT, 4, movetoworkspacesilent, 4
bind = $mainMod SHIFT, 5, movetoworkspacesilent, 5
bind = $mainMod SHIFT, 6, movetoworkspacesilent, 6
bind = $mainMod SHIFT, 7, movetoworkspacesilent, 7
bind = $mainMod SHIFT, 8, movetoworkspacesilent, 8
bind = $mainMod SHIFT, 9, movetoworkspacesilent, 9
bind = $mainMod SHIFT, 0, movetoworkspacesilent, 10

#   █▀▄▀█ █▀█ █░█ █▀ █▀▀   █▄▄ █ █▄░█ █▀▄ █ █▄░█ █▀▀
#   █░▀░█ █▄█ █▄█ ▄█ ██▄   █▄█ █ █░▀█ █▄▀ █ █░▀█ █▄█
# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1
# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow
